Questions!

TestIterator
// TODO also try with a LinkedList - does it make any difference?
LinkedLists will work for this case. ArrayLists and LinkedLists are both implementations of the
List interface and maintains insertion order. ArrayList is dynamic while LinkedList uses a doubly linked list. ArrayLists are more
difficult to manipulate though while LinkedLists are not so insertion and removal is a bit faster using a LinkedList.

// TODO what happens if you use list.remove(Integer.valueOf(77))?
It's trying to remove the object value of 77 from the list but that causes the test to fail.

TestList
// TODO also try with a LinkedList - does it make any difference?
LinkedLists will work in this case also. As mentioned before, LinkedLists are better for manipulation of data while ArrayLists are better
for storing data. Since many test cases involve insertion and removal of values, LinkedLists are actually better to use.

// TODO answer: what does this method do? (line 106)
Calling remove(5) removes Integer 77 from list at index 5.

TestPerformance
All tests were executed 5 times for each SIZE (10, 100, 1000 and 10000) and
the running time is in milliseconds. The running time was recorded using
System.currentTimeMillis()

	SIZE 10

    testArrayListAddRemove:  26; 31; 30; 27; 37
    testLinkedListAddRemove: 38; 39; 40; 45; 41
	testArrayListAccess:     15;  15; 11; 11; 17
    testLinkedListAccess:    12;  14; 16; 16; 16

	SIZE 100

    testArrayListAddRemove:  49; 47; 44; 46; 58;
    testLinkedListAddRemove: 32; 46; 53; 37; 33;
	testArrayListAccess:     9;  11;  12;  10;  11
    testLinkedListAccess:    28;  37;  26;  34;  32

	SIZE 1000

    testArrayListAddRemove:  140;  157;  137;  156; 151;
    testLinkedListAddRemove: 40;  36;  33;  38; 26
	testArrayListAccess:     13;   12;   9;   13; 12;
    testLinkedListAccess:    279; 310; 319; 307; 300;

	SIZE 10000

    testArrayListAddRemove:  1428;  1391;  1386;  1389;  1421;
    testLinkedListAddRemove: 35;   33;   35;   34;   50;
	testArrayListAccess:     47;    44;    43;    43;    52
    testLinkedListAccess:    3894; 3765; 3804; 3838; 3932;

// TODO answer: which of the two lists performs better as the size increases?
As the size increases, LinkedList performs better in regards to adding and removing elements from the list. In terms of accessing an element
an ArrayList performs better. The data from the runtime above is agreeable with this conclusion. ArrayLists are indexed thus getting a value
is at a time complexity of O(1) but insertion and removal are O(n). For a LinkedList add and remove are O(1) but accessing are at O(n). We also
notice that when the size is "small" (relatively) the runtime is more similar.

